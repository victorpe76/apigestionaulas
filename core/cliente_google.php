<?php 
require_once 'config.php';
require_once '../libs/google-api-php-client/vendor/autoload.php';
require_once 'modeldb.php';

define('APPLICATION_NAME', 'App Gestión de Aula ECI');
define('CREDENTIALS_PATH', '~/.credentials/calendar_appgestionaulas.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
define('ARCHIVO_SQL',__DIR__ . '/recursos/appgestionaulas.sql');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/calendar-php-quickstart.json
define('SCOPES', 'https://www.googleapis.com/auth/calendar');

class ClienteGoogle {
    private $service;
    private $dias=array('SU','MO','TU','WE','TH','FR','SA');

    function __construct() {
     if($this->comprobarBaseDedatos()){
      $this->service=new Google_Service_Calendar($this->getClient());;
     }
       

    }

    
    function crearAula($idaula){
        $calendar = new Google_Service_Calendar_Calendar();
        $calendar->setSummary($idaula);
        $createdCalendar = $this->service->calendars->insert($calendar);
        return $createdCalendar->getId();
    }

    function borrarAula($idaula){
      $this->service->calendars->delete($idaula);
      
  }

  function ocupacion($idaula,$desde,$hasta){
    $peticion=new Google_Service_Calendar_FreeBusyRequest(array(
      'timeMin'=>$desde.'T00:00:00+01:00',
      'timeMax'=>$hasta.'T23:59:59+01:00',
      'timeZone'=>'Europe/Madrid',
      'items'=>array(array('id'=>$idaula))
    ));
    $resultado=$this->service->freebusy->query($peticion);
    return $resultado["calendars"][$idaula]["busy"];
  }
  function borrarAlquiler($alquiler,$aulas){
    //Primero obtenemos la lista de aulas
   // print_r($aulas);
   
    $options=array('maxResults'=>2000,'q'=>$alquiler,'singleEvents'=>true,'timeZone'=>'Europe/Madrid');
    foreach($aulas as $k => $v){
      $calendarid=$v["id"];
      $v["tarifas"]=json_decode(stripslashes($v["tarifas"]));
      $eventos=$this->service->events->listEvents($calendarid,$options);
      //print_r($eventos);
     if(sizeof($eventos["items"])>0){
        $retorno_aula=array('horario'=>array(),'aula'=>$v);
        

              foreach($eventos["items"] as $ke => $ve){
                //print_r($ve);
               $this->service->events->delete($calendarid,$ve->id);
                
              }
        return $retorno_aula;
      }
      

      
      
    }

    
    
  }

  function obtenerAlquiler($alquiler,$aulas){
    //Primero obtenemos la lista de aulas
   // print_r($aulas);
   
    $options=array('maxResults'=>2000,'q'=>'$alquiler','singleEvents'=>true,'timeZone'=>'Europe/Madrid');
    foreach($aulas as $k => $v){
      $calendarid=$v["id"];
      $v["tarifas"]=json_decode(stripslashes($v["tarifas"]));
      $eventos=$this->service->events->listEvents($calendarid,$options);
      if(sizeof($eventos["items"])>0){
      $retorno_aula=array('horario'=>array(),'aula'=>$v);

      $datos_alquiler=explode(";",$eventos["items"][0]["description"]);
      
      foreach($datos_alquiler as $kd => $kv){
        $pareja=explode(":",$kv);
        $retorno_aula[$pareja[0]]=$pareja[1];
      }

      
      
      foreach($eventos["items"] as $ke => $ve){
        
        //print_r($ve->start->dateTime);
        //$ev=array()
        array_push($retorno_aula["horario"],
          array('inicio'=>$ve->start->dateTime,
                'fin'=>$ve->end->dateTime
              ));
        
      }

      return $retorno_aula;
    }
      
    }

    
    
  }

  function obtenerAlquileresAulaPeriodo($calendarid,$desde,$hasta){
    $retorno=array();
    $arrayalquileres=array();
    $options=array('maxResults'=>2000,'q'=>$cliente,
    'singleEvents'=>true,
    'timeMin'=>$desde.'T00:00:00+01:00',
    'timeMax'=>$hasta.'T23:59:59+01:00',
    'timeZone'=>'Europe/Madrid');
    $eventos=$this->service->events->listEvents($calendarid,$options);
    if(sizeof($eventos["items"])>0){

      $arrayalquileres[$eventos["items"][0]["description"]]=array('horario'=>array());

    

      $arrayalquileres[$eventos["items"][0]["description"]]
          =array_merge($arrayalquileres[$eventos["items"][0]["description"]],$this->extraerDatosDescripcion($eventos["items"][0]["description"]));
    

    
    
    foreach($eventos["items"] as $ke => $ve){

      if(!array_key_exists($ve->description,$arrayalquileres)){
        //Si este evento no existe
        //lo añadimos
        $arrayalquileres[$ve->description]=array('horario'=>array());
        $arrayalquileres[$ve->description]
                  =array_merge($arrayalquileres[$ve->description],$this->extraerDatosDescripcion($ve->description));
            
        
        
      }
      //Simplemente añadimos el horario
      array_push($arrayalquileres[$ve->description]['horario'],
      array('start'=>$ve->start->dateTime,
      'end'=>$ve->end->dateTime
    ));
      
      
    }

    
  }
    foreach($arrayalquileres as $k => $v){
      array_push($retorno,$v);
    }
   return $retorno;
  }

  function obtenerAlquilerAula($idaula,$idalquiler){
    $retorno_aula=array();
    $options=array('maxResults'=>2000,'q'=>$idalquiler,'singleEvents'=>true,'timeZone'=>'Europe/Madrid');
    $eventos=$this->service->events->listEvents($idaula,$options);
    if(sizeof($eventos["items"])>0){

    $retorno_aula=array('horario'=>array(),
    'description'=>$eventos["items"][0]["description"]);

      $retorno_aula=array_merge($retorno_aula,$this->extraerDatosDescripcion($eventos["items"][0]["description"]));
    

    
    
    foreach($eventos["items"] as $ke => $ve){
      
      
      array_push($retorno_aula["horario"],
        array('start'=>$ve->start->dateTime,
              'end'=>$ve->end->dateTime
            ));
      
    }
  }
    return $retorno_aula;
  
  }

  function obtenerAlquileres($cliente,$aulas){
    //Primero obtenemos la lista de aulas
   // print_r($aulas);
    $retorno=array();
    $options=array('maxResults'=>2000,'q'=>$cliente,'singleEvents'=>true,'timeZone'=>'Europe/Madrid');
    foreach($aulas as $k => $v){
      $calendarid=$v["id"];
      $v["tarifas"]=json_decode(stripslashes($v["tarifas"]));
      $eventos=$this->service->events->listEvents($calendarid,$options);
      if(sizeof($eventos["items"])>0){

      $retorno_aula=array('horario'=>array(),'aula'=>$v,
      'description'=>$eventos["items"][0]["description"]);

        $retorno_aula=array_merge($retorno_aula,$this->extraerDatosDescripcion($eventos["items"][0]["description"]));
      

      
      
      foreach($eventos["items"] as $ke => $ve){
        
        //print_r($ve->start->dateTime);
        //$ev=array()
        if($ve->description != $retorno_aula['description']){
          array_push($retorno,$retorno_aula);
          $retorno_aula=array('horario'=>array(),'aula'=>$v,
          'description'=>$ve->description);
          $retorno_aula=array_merge($retorno_aula,
          $this->extraerDatosDescripcion($ve->description));
          
        }
        array_push($retorno_aula["horario"],
          array('inicio'=>$ve->start->dateTime,
                'fin'=>$ve->end->dateTime
              ));
        
      }

      array_push($retorno,$retorno_aula);
    }
      
    }

    return $retorno;
    
  }

  function extraerDatosDescripcion($descripcion){
    $datos_alquiler=explode(";",$descripcion);
    $retorno = array();
    foreach($datos_alquiler as $kd => $kv){
      $pareja=explode(":",$kv);
      $retorno[$pareja[0]]=$pareja[1];
    }
    return $retorno;
  }


  function comprobarBaseDedatos(){
    try{
    $modelo=new modeldb();
   $data=$modelo->get_query("SHOW TABLES LIKE 'configuracion'"); 
   array_pop($data);
   //print_r($data);
    if(count($data)>0){
      return true;
    }
  }catch(Exception $e){
    return $this->crearBaseDatos();
  }
    return $this->crearBaseDatos();
  }
  function crearBaseDatos(){
    $modelo=new modeldb();
    $commands = file_get_contents(ARCHIVO_SQL);
    $modelo->conn->multi_query($commands);
   // print($commands);
   return $this->comprobarBaseDedatos();
  }
  function crearAlquiler($datos){
    $resultado=0;
    foreach($datos['horarios'] as $k => $v){
      $rdias=array();
      foreach($v['recurrencia'] as $kr => $vr){
       // echo 'DIA: '.$vr;
        array_push($rdias,$this->dias[$vr]);
      }
      $fin=$datos['fin']."T".$v['hasta'];
      $fin=str_replace('-','',$fin);
      $fin=str_replace(':','',$fin);

    $datosevento=array(
      'summary' => $datos['ref'],
      'description' => 'id:'.$datos['id']
                      .';tarifa:'.$datos['tarifa']
                      .';cliente:'.$datos['cliente']
                      .';inicio:'.$datos['inicio']
                      .';fin:'.$datos['fin']
                      .';ref:'.$datos['ref'],
      'start' => array(
        'dateTime' => $datos['inicio']."T".$v['desde'].'+01:00',
        'timeZone' => 'Europe/Madrid',
      ),
      'end' => array(
        'dateTime' => $datos['inicio']."T".$v['hasta'].'+01:00',
        'timeZone' => 'Europe/Madrid',
      ),
      'recurrence' => array(
        'RRULE:FREQ=WEEKLY;BYDAY='.join(',',$rdias).";UNTIL=".$fin.'z',
      ),
      'attendees' => array(),
      'reminders' => array(
        'useDefault' => FALSE,
        ),
      );
      //print_r($datosevento);
    $event = new Google_Service_Calendar_Event($datosevento);
    
    $calendarId = $datos['idaula'];
    $event = $this->service->events->insert($calendarId, $event);
    if($event->status=='confirmed'){
      $resultado++;
    }
    //printf('Event created: %s\n', $event->htmlLink);
    }
if($resultado==sizeof($datos["horarios"])){
  return true;
}
    return false;
    /*
    //print_r($datos);
    $datosevento=array(
      'summary' => $datos['ref'],
      'description' => 'id:'.$datos['id'].';tarifa:'.$datos['tarifa'].';cliente:'.$datos['cliente'],
      'start' => array(
        'dateTime' => '2018-05-28T09:00:00-07:00',
        'timeZone' => 'America/Los_Angeles',
      ),
      'end' => array(
        'dateTime' => '2018-05-28T17:00:00-07:00',
        'timeZone' => 'America/Los_Angeles',
      ),
      'recurrence' => array(
        'RRULE:FREQ=DAILY;COUNT=2'
      ),
      'reminders' => array(
        'useDefault' => FALSE,
      ) 
      );

      print_r($datosevento);
      $event = new Google_Service_Calendar_Event($datosvento);
      print_r($event);
      $this->service->events->insert($datos['idaula'], $event);
      /*
      //echo '</br>DATOS INICIALES:';
      //print_r($datosevento);
      foreach($datos['horarios'] as $k => $v){
        $datosevento['start']=array('dateTime'=>$datos['inicio']."T".$v['desde'].'+01:00');
        $datosevento['end']=array('dateTime'=>$datos['inicio']."T".$v['hasta'].'+01:00');
        //Vamos a generar los dias
        $rdias=array();
        foreach($v['recurrencia'] as $kr => $vr){
         // echo 'DIA: '.$vr;
          array_push($rdias,$this->dias[$vr]);
        }
        $fin=$datosevento['end']['dateTime'];
        $fin=str_replace('-','',$fin);
        $fin=str_replace(':','',$fin);
        $datosevento['recurrence'] = array('RRULE:FREQ=WEEKLY;BYDAY='.join(',',$rdias).";UNTIL=".$fin);
        print_r($datosevento);
        $event = new Google_Service_Calendar_Event($datosvento);
        $this->service->events->insert($datos['idaula'], $event);
        
      }
*/
    //$event = new Google_Service_Calendar_Event($datosvento);
    
    //$calendarId = 'primary';
    //$event = $service->events->insert($calendarId, $event);
  }

   
  public function obtenerLinkAutorizacion(){
    if($this->obtenerCredencialesAcceso()!=null){
     // print("existen las credenciales de acceso");
      return null;
    }
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');
    $client->setDeveloperKey(API_KEY);

    $authUrl = $client->createAuthUrl();
    $retorno=array('googlelink' => stripslashes($authUrl));

    return $retorno;

  }

  public function obtenerCredencialesConCodigo($codigo){
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');
    $client->setDeveloperKey(API_KEY);
    $accessToken = $client->fetchAccessTokenWithAuthCode($codigo);
   
    $this->establecerCredencialesAcceso(json_encode($accessToken));
    return true;
  }

private function establecerCredencialesAcceso($datos){
    $datos=addslashes($datos);
    
    $modelo=new modeldb();
    $modelo->set_query("DELETE FROM configuracion WHERE clave LIKE 'google_access_token'");
    $sentencia="INSERT INTO configuracion (clave,valor) VALUES('google_access_token','".$datos."')";
    
    $modelo->set_query($sentencia);

  }

  private function obtenerCredencialesAcceso(){
    $modelo=new modeldb();
    $result=$modelo->get_query("SELECT * FROM configuracion WHERE clave LIKE 'google_access_token'");
    if(sizeof($result)>0){
      return json_decode(stripslashes($result[0]['valor']));
    }else{
      return null;
    }
    
  }

   private function getClient() {
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');
        $client->setDeveloperKey(API_KEY);
        
        $credenciales=$this->obtenerCredencialesAcceso();
        if($credenciales!=null){
        $accessToken = json_encode($credenciales);
        

        $client->setAccessToken($accessToken);
      
        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
          $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
          $this->establecerCredencialesAcceso(json_encode($client->getAccessToken()));
          
          
        
        }
      }
        return $client;
      }
}



