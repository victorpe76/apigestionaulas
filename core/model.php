<?php 
 // Se incluye el archivo de conexion de base de datos
 include 'modeldb.php';
 // Se incluye la interfaz de Modelo
 include 'imodel.php';

 // Se crea la clase que ejecuta llama a las funciones de ejecución para interactuar con la Base de datos
 // Esta clase extiende a la clase modeldb en el archivo modeldb.php (hereda sus propiedades y metodos)
 // Esta clase implementa la interfaz iModel (Enmascara cada una de las funciones declaradas)
 class Modelo extends modeldb implements imodel {
  // Ya que la clase es generica, es importante poseer una variable que permitira identificar con que tabla se trabaja
  public $entity;
  // Almacena la informacion que sera enviada a la Base de datos
  public $data;
  
  // Esta funcion se activara al utilizar el metodo GET
  // Envia por defecto el parametro Id cuyo valor sera 0 hasta que se modifique
  function get($id = 'vacio') {
   /* 
    * Si el valor del parametro Id es igual a 0, se solicitaran todos los elementos
    * ya que no se ha solicitado un elemento especifico 
    */
   if($id == 'vacio') {
    return $this->get_query(sprintf("
     SELECT 
      * 
     FROM 
      %s", 
      $this->entity
      )
     );
   // Si el valor del parametro Id es diferente a 0, se solicitara solo y unicamente el elemento cuyo Id sea igual al parametro recibido
   } else {
    return $this->get_query(sprintf("
     SELECT 
      * 
     FROM 
      %s 
     WHERE 
      Id LIKE '%s'", 
      $this->entity, 
      $id
      )
     );
   }
  }

  // Esta funcion sera llamada al momento de usar el metodo POST
  function post() {

   return $this->set_query(sprintf("
    INSERT INTO 
     %s
     %s",
     $this->entity,
     $this->data
     
    )
   );

   
  }

  // Esta funcion sera llamada al momento de usar el metodo PUT
  function put($id = 'vacio') {
   return $this->set_query(sprintf("
    UPDATE 
     %s 
    SET 
     %s 
    WHERE 
     Id LIKE '%s'", 
     $this->entity,
     $this->data, 
     $id
    )
   );

  }

  // Esta funcion sera llamada al momento de usar el metodo DELETE
  function delete($id = 'vacio') {
   return $this->set_query(sprintf("
    DELETE FROM 
     %s 
    WHERE 
     Id LIKE '%s'", 
     $this->entity,
     $id
    )
   );

  }
 }

 function get_model(){
     return new Modelo;
 }

  // Esta funcion renderiza la informacion que sera enviada a la base de datos
  function renderizeData($keys, $values) {
    
      switch ($_SERVER['REQUEST_METHOD']) {
       case 'POST':
        # code...
         foreach ($keys as $key => $value) {
          if($key == count($keys) - 1) {
           $str = $str . $value . ") VALUES (";
    
           foreach ($values as $key => $value) {
            if($key == count($values) - 1) {
             $str = $str . "'" . $value . "')";
            } else {
             $str = $str . "'" . $value . "',";
            }
            
           }
          } else {
           if($key == 0) {
            $str = $str . "(" . $value . ",";
           } else {
            $str = $str . $value . ",";
           }
           
          }
         }
    
         return $str;
        break;
       case 'PUT':
        foreach ($keys as $key => $value) {
         if($key == count($keys) - 1) {
          $str = $str . $value . "='" . $values[$key] . "'"; 
         } else {
          $str = $str . $value . "='" . $values[$key] . "',"; 
         }
        }
        return $str;
        break;
      }
      
    
    
     }
    
     // Esta funcion imprime las respuesta en estilo JSON y establece los estatus de la cebeceras HTTP
     function print_json($status, $mensaje, $data) {
      header("HTTP/1.1 $status $mensaje");
      header("Content-Type: application/json; charset=UTF-8");
    
     
      $response['statusCode'] = $status;
      $response['statusMessage'] = $mensaje;
      $response['data'] = $data;
      
      echo json_encode($data, JSON_PRETTY_PRINT);
     }

     //Esta funcion genera un UUID v4
     function gen_uuid()
     {
       $data=openssl_random_pseudo_bytes(16);
         assert(strlen($data) == 16);
     
         $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
         $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
     
         return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
     }
?>