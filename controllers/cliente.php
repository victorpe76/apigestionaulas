<?php
include '../core/enable_cors.php';

require_once '../core/model.php';

//Obtenemos el id

$id=$_GET["id"];
//Obtenemos el body de la peticion
$bodyRequest = file_get_contents("php://input");

//Obtenemos un instancia del modelo
$modelo=get_model();
//Establecemos la entidad
$modelo->entity="cliente";



switch ($_SERVER['REQUEST_METHOD']) {
    
    case "GET":
        if(isset($id)){
            
            //Obtenemos un cliente concreto
            $data=$modelo->get($id);
        }else{
            //Mostramos todos los clientes
            $data=$modelo->get();
        }

        // Elimina el ultimo elemento del array $data, ya que usualmente, suele traer dos elementos, uno con la informacion, y otro NULL el cual no necesitamos
        array_pop($data);
        if(count($data)==0) {
            print_json(404, "Cliente no encontrado", null);
        }else{
            if(isset($id)){
                $data=$data[0];
            }
            print_json(200, "OK", $data);
        }
    break;

    case "POST":
    
        //GEneramos un uuid
        $uuid=gen_uuid();
        $array = json_decode($bodyRequest, true);
        $array["id"]=$uuid;
        $modelo->data = renderizeData(array_keys($array), array_values($array));
        $data = $modelo->post();
        if($data) {

            //print_json(201,"Cliente ",$data);
            $datan=$modelo->get($uuid);
            if(count($datan)==0) {
                
                print_json(400, "Cliente erroneo", null);
               // Si la variable $data es diferente de 0, el elemento ha sido creado y manda la siguiente respuesta
               } else {
                
                print_json(201, "Cliente creado", $array);
               }
        }else{
            print_json(400, "Cliente erroneo", null);
        }
        

    break;

    case "PUT":

    $data=$modelo->get($id);
    array_pop($data);
    if(count($data)==0) {
        print_json(404, "Cliente no encontrado", null);
        return;
    }

            if(isset($id)){
                $array = json_decode($bodyRequest, true);
                $modelo->data = renderizeData(array_keys($array), array_values($array));
                $data = $modelo->put($id);
                if($data) {
                    print_json(201, "Cliente modificado", $array);
                                   
                            }else{
                                print_json(400, "Cliente erroneo", null);
                            }
            }
    break;

    case "DELETE":
    $data=$modelo->get($id);
    array_pop($data);
    if(count($data)==0) {
        print_json(404, "Cliente no encontrado", null);
        return;
    }
    if(isset($id)){

        $data = $modelo->delete($id);
        if($data) {
            print_json(201, "Cliente eliminado", $id);
                           
                    }else{
                        print_json(404, "Cliente no encontrado", null);
                    }
    }
    break;
}



