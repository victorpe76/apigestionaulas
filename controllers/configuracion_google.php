<?php

include '../core/enable_cors.php';

require_once '../core/model.php';

require_once '../core/cliente_google.php';

//Obtenemos una instancia de GoogleConector
$googleconnector=new ClienteGoogle();

//Obtenemos el body de la peticion
$bodyRequest = file_get_contents("php://input");

switch ($_SERVER['REQUEST_METHOD']) {
    
    case "GET":
    
    
       // $googleconnector->comprobarBaseDedatos();
        
        $datos=$googleconnector->obtenerLinkAutorizacion();
        //print_r($datos);
        if($datos!=null){
            //print("Vamos a devolver las credenciales");
            print_json(200,"Link de google",$datos);
        }else{
            print_json(401, "Las credenciales ya han sido establecidas",null);  
        }
        
   
       
    
        break;

    case "POST":

        if($googleconnector->obtenerCredencialesConCodigo($bodyRequest)){
            print_json(201, "Credenciales establecidas correctamente",null);  
        }
    
        break;

    
}