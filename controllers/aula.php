<?php
include '../core/enable_cors.php';

require_once '../core/model.php';
require_once '../core/cliente_google.php';
//Obtenemos el id

$id=$_GET["id"];
$desde=$_GET["desde"];
$hasta=$_GET["hasta"];
$idalquiler=$_GET["idalquiler"];
//Obtenemos el body de la peticion
$bodyRequest = file_get_contents("php://input");

//Obtenemos un instancia del modelo
$modelo=get_model();
//Obtenemos una instancia de GoogleConector
$googleconnector=new ClienteGoogle();

//Establecemos la entidad
$modelo->entity="aula";



switch ($_SERVER['REQUEST_METHOD']) {
    
    case "GET":
    
        if(isset($id)){
            if(isset($hasta) && isset($desde)){
                //Estamos solicitando los eventos del aula en un periodo concreto
                $data=$googleconnector->obtenerAlquileresAulaPeriodo($id,$desde,$hasta);
                print_json(201, "Alquileres",$data);
                return;  
            }else if(isset($idalquiler)){
                $data=$googleconnector->obtenerAlquilerAula($id,$idalquiler);
                print_json(201, "Alquiler",$data);
                return; 
             
            }else{
                //Obtenemos un cliente concreto
                $data=$modelo->get($id);
            }
            
        }else{
            //Mostramos todos los clientes
            $data=$modelo->get();

        }

        // Elimina el ultimo elemento del array $data, ya que usualmente, suele traer dos elementos, uno con la informacion, y otro NULL el cual no necesitamos
        array_pop($data);


        if(count($data)==0) {
            print_json(404, "Not Found", null);
        }else{
            foreach($data as $k => $v){
                $data[$k]["tarifas"]=json_decode(stripslashes($v["tarifas"]));
               //print_r($v["tarifas"]);
            }
            //print_r($data);

            if(isset($id)){

                $data=$data[0];
                
            }
            print_json(200, "OK", $data);
        }
    break;

    case "POST":
        //GEneramos un uuid
        
        $array = json_decode($bodyRequest, true);
        
        $array["id"]=$googleconnector->crearAula($array["nombre"]);
        
        $uuid=$array["id"];
       // $array["id"]=$uuid;
        //Modificamos la tarifa
        
        $tarifas=json_encode($array["tarifas"]);

        $array["tarifas"]=addslashes( $tarifas);
        //Vamos a guardar el aula en calendar
       
        $modelo->data = renderizeData(array_keys($array), array_values($array));
        $data = $modelo->post();
        if($data) {

            //print_json(201,"Cliente ",$data);
            $datan=$modelo->get($uuid);
            if(count($datan)==0) {
                
                print_json(400, "Aula erronea", null);
                $googleconnector->borrarAula($uuid);
               // Si la variable $data es diferente de 0, el elemento ha sido creado y manda la siguiente respuesta
               } else {
                
                print_json(201, "Aula creada", $array);
               }
        }else{
            print_json(400, "Aula erronea", null);
            $googleconnector->borrarAula($uuid);
        }
        

    break;

    case "PUT":
            if(isset($id)){
                $array = json_decode($bodyRequest, true);

                //Modificamos la tarifa
        
                $tarifas=json_encode($array["tarifas"]);
        
                $array["tarifas"]=addslashes( $tarifas);

                $modelo->data = renderizeData(array_keys($array), array_values($array));
                $data = $modelo->put($id);
                if($data) {
                    print_json(201, "Aula modificada", $array);
                                   
                            }else{
                                print_json(400, "Aula erronea", null);
                            }
            }
    break;

    case "DELETE":
    if(isset($id)){

        $googleconnector->borrarAula($id);
        $data = $modelo->delete($id);

        if($data) {
            print_json(201, "Aula eliminada", $id);
                           
                    }else{
                        print_json(400, "Aula erronea", null);
                    }
    }
    break;
}



