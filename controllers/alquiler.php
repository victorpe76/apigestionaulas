<?php
include '../core/enable_cors.php';
require_once '../core/model.php';
require_once '../core/cliente_google.php';

//Obtenemos el id

$id=$_GET["id"];

$idalquiler=$_GET["idalquiler"];
//Obtenemos el body de la peticion
$bodyRequest = file_get_contents("php://input");

//Obtenemos un instancia del modelo
$modelo=get_model();
//Obtenemos una instancia de ClienteGoogle
$googleconnector=new ClienteGoogle();



switch ($_SERVER['REQUEST_METHOD']) {
    
    case "GET":
    $modelo->entity="cliente";
    $data=$modelo->get($id);
    array_pop($data);
    if(count($data)==0) {
        print_json(404, "Cliente no encontrado", null);
        return;
    }
    //Obtenemos las aulas
    $modelo->entity="aula";
    $data=$modelo->get();
    array_pop($data);
    if($idalquiler!=null){
        $datos=$googleconnector->obtenerAlquiler($idalquiler,$data);
        print_json(201, "Alquileres",$datos);  
    }else{
        $datos=$googleconnector->obtenerAlquileres($id,$data);
        print_json(201, "Alquileres",$datos);  
    }
       
    
        break;

    case "POST":
    $modelo->entity="cliente";
    $data=$modelo->get($id);
    array_pop($data);
    if(count($data)==0) {
        print_json(404, "Cliente no encontrado", null);
        return;
    }
        $uuid=gen_uuid();
        $array = json_decode($bodyRequest, true);
        $array["id"]=$uuid;
        $array["cliente"]=$id;
        if($googleconnector->crearAlquiler($array)){
            print_json(201, "Alquiler creado",$array);   
        }else{
            print_json(400, "Alquiler no creado",null);   
        }
    break;

    case "PUT":
            
    break;

    case "DELETE":
    if($idalquiler!=null){
        $modelo->entity="aula";
        $data=$modelo->get();
        array_pop($data);
        $datos=$googleconnector->borrarAlquiler($idalquiler,$data);
        print_json(201, "Alquiler eliminado",$datos); 
    }
    
    break;
}



