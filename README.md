# RESTFUL API GESTION DE AULAS

Esta es la API RESTFUL de la aplicación GESTIÓN DE AULAS como proyecto de Desarrollo de aplicación web perteneciente el ciclo de grado superior de FP DESARROLLO DE APLICACIONES WEB del CIFP PONFERRADA
- Autor: Víctor Pérez Tapia
- Curso 2017/2018

## INSTALACIÓN

La correcta instalación de esta API debe contemplar los siguientes pasos
- Instalación de la BBDD en servidor MYSQL
- Obtención de credenciales en Google Cloud Console
- Configuración del archivo `core/config.php`

### Instalación de la BBDD

Utilizar el script SQL contenido en el archivo `recursos/appgestionaulas.sql` para crear la estructura de la base de datos necesaria para el proyecto

### Credenciales en Google Cloud Console

Para obtener las credenciales necesarias para que la aplicación pueda realizar operaciones con la api de Google Calendar seguir las indicaciones contenidas en el paso 1 de la [Guia Rápida PHP](https://developers.google.com/google-apps/calendar/quickstart/php)
Recuerde copiar el archivo `client_secret.json` en el directorio `controllers` del proyecto

Además deberá crear una API KEY en su proyecto en Google Cloud. Para esta operación siga las indicaciones de [Usando API KEYS](https://developers.google.com/api-client-library/php/auth/api-keys).
Copia la API KEY generada para establecerla como constante en el archivo `core/config.php``

### Configuración del archivo `core/config.php``

Establezca los valores correspondientes para las constantes que figuran en el archivo `core/config.php`

## USO de la api

Esta API ha sido diseñada utilizando el editor SWAGGERHUB y se encuentra documentada en [API GESTION AULAS](https://app.swaggerhub.com/apis/victoraidiapp/GestionAulasECI/0.0.1)
